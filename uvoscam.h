/**
* uvoscam
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include <string>
#include <functional>
#include <condition_variable>
#include <stdint.h>
#include <thread>
#include <atomic>
#include <opencv2/core/mat.hpp>

typedef struct _ArvStream ArvStream;
typedef struct _ArvBuffer ArvBuffer;
typedef struct _ArvCamera ArvCamera;

namespace cam
{
	
class Camera
{
public:
	
	class Description
	{
	private:
		uint64_t hash = 0;
		std::string vendor_;
		std::string serial_;
		std::string model_;
		std::string id_;
	public:
		
		Description(std::string const vendor, const std::string model, const std::string serial, const std::string id = ""): 
		vendor_(vendor), serial_(serial), model_(model), id_(id)
		{
			hash = std::hash<std::string>{}(vendor_ + serial_ + model_);
		}
		
		uint64_t getId() const
		{
			return hash;
		}
		
		std::string getIdString() const
		{
			return id_;
		}
		
		std::string getVendor() const
		{
			return vendor_;
		}
		
		std::string getSerial() const
		{
			return serial_;
		}
		
		std::string getModel() const
		{
			return model_;
		}
		
		bool operator==(const Description& in) const
		{
			return in.getId() == getId();
		}
		bool operator!=(const Description& in) const
		{
			return !operator==(in);
		}
	};
	
	static constexpr size_t BUFFER_DEPTH = 8;
	
	typedef int TriggerMode;
	static constexpr TriggerMode TRIGGER_FREE     = 0;
	static constexpr TriggerMode TRIGGER_SOFTWARE = 1;
	static constexpr TriggerMode TRIGGER_HARDWARE = 2;
	
	typedef int BayerMode;
	static constexpr BayerMode BAYER_RED        = 0;
	static constexpr BayerMode BAYER_GREEN      = 1;
	static constexpr BayerMode BAYER_BLUE       = 2;
	static constexpr BayerMode BAYER_PASSTHOUGH = 3;
	static constexpr BayerMode BAYER_DEBAYER    = 4;

	
	typedef int AquisitionMode;
	static constexpr AquisitionMode MODE_FREE   = 0;
	static constexpr AquisitionMode MODE_SINGLE = 1;
	static constexpr AquisitionMode MODE_DISCARD = 2;
	
private:
	ArvStream* aStream_ = nullptr;
	ArvCamera* aCamera_ = nullptr;
	
	Description* description = nullptr;
	
	unsigned int width_;
	unsigned int height_;
	int64_t format = -1;
	inline static bool scanned_ = false;
	
	ArvBuffer* imageBuffers[BUFFER_DEPTH] = {0};
	
	BayerMode bayerMode = BAYER_DEBAYER;
	
	ArvBuffer* decodeBuffer = nullptr;
	std::mutex decodeBufferMutex;
	std::condition_variable bufferCondition;
	std::mutex bufferConditionMutex;
	std::thread* decoderThread = nullptr;
	std::atomic_bool join;
	
	std::atomic_bool run;
	
	AquisitionMode aqmode = MODE_FREE;
	int discardCount = 0;
	bool aqRunning = false;
	
	void decoderThreadFunc();
	
	std::function<void(cv::Mat)> callback_;
	
	bool setupCamera();
	
	bool chooseFormat();
	
	cv::Mat debayerBG(ArvBuffer *buffer);
	cv::Mat debayerGB(ArvBuffer *buffer);
	cv::Mat pack8(ArvBuffer *buffer, bool color);
	cv::Mat pack16(ArvBuffer *buffer);
	
public:
	
	static std::vector<Camera::Description> getAvailableCameras(bool update = false);
	
	Camera(std::function<void(cv::Mat)> callback);
	bool openCamera(const std::string& name);
	bool openCamera(const Camera::Description& camera);
	bool openCamera(const size_t index);
	~Camera();
	bool isOpen();
	bool setAcquisitionMode(AquisitionMode mode);
	bool setGain(double gain);
	double getGain();
	bool setExposureTime(uint64_t us);
	bool getExposureTimeLimits(uint64_t& min, uint64_t& max);
	uint64_t getExposureTime();
	double getFrameRate();
	bool setFrameRate(double rate);
	int64_t getPixelFormat();
	bool setExposureAuto(bool enable);
	bool hasExposureAuto();
	bool startAcquisition();
	bool stopAcquisition();
	bool setMtu(int mtu);
	bool getTemperature(double& temperature);
	void getSize(unsigned int* x, unsigned int* y);
	void setBayerMode(Camera::BayerMode mode);
	Camera::BayerMode getBayerMode();
	void setTriggerMode(Camera::TriggerMode mode);
	void trigger();
	Camera::Description getDescription();
	
	static void aCallback(void* instance, int type, ArvBuffer *buffer);
};

}
