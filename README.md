UVOS libuvoscam and uvoscam_cli
==============

This is a library and cli application that wraps aravis to avoid GOBJECT leakage into a c++ application using aravis. On top of this it also implements color space conversions and demosaicing  
license: GPL v3.0 or later

Building and Installing
----------

Requirements:

* cmake
* pkg-config
* openCV v3.0 or later
* aravis-0.8

Build instructions:

1. run 'cmake .'
2. run 'make'

To install run 'make install' as root.


uvoscam_cli usage
----------

To unwrap a object:

1. run 'uvoscam_cli'
2. at the > prompt type 'list' to show the cameras available
3. open a camera with 'open $(index)' 
4. use 'capture $(imageFileName)' to save an image
5. for more available commands type 'help'
