/**
* Cam
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <math.h> 
#include <vector>
#include <string>
#include <sstream>
#include <opencv2/imgcodecs.hpp>

#include "log.h"
#include "uvoscam.h"

#define VERSION "0.1"

using namespace cam;

static std::string filename;

void listDevices()
{
	std::vector<Camera::Description> cameras = Camera::getAvailableCameras();
	size_t i = 0;
	for(auto camera : cameras)
		std::cout<<i++<<": "<<camera.getId()<<' '<<camera.getModel()<<' '<<camera.getVendor()<<'\n';
}

void help()
{
	std::cout<<"Avaiable commands:\n"
	         <<"help            Displays this message\n"
	         <<"list            Lists cameras found\n"
	         <<"open [name]     Open camera with string name\n"
	         <<"close           Close camera \n"
	         <<"capture [file]  Take an image and save it to file\n"
	         <<"exposure [s]    set exposure to x seconds\n"
	         <<"gain [n]        set gain to n times\n"
	         <<"exit            Quit program\n";
}

void callback(cv::Mat data)
{
	std::cout<<"Saveing image\n> "<<std::flush;
	cv::imwrite(filename, data);
}

std::vector<std::string> tokenize(const std::string& in, char delim = ' ')
{
	std::vector<std::string> vect;
	std::stringstream ss(in);
	
	for(std::string token; std::getline(ss, token, delim);)
		vect.push_back(token);
	return vect;
}

int main(int argc, char* argv[])
{	
	Camera camera(callback);
	
	Log::level = Log::DEBUG;
	
	Log(Log::INFO)<<"UVOS Camera capture program "<<VERSION;

	while(true)
	{
		std::cout<<"> ";
		std::string line;
		std::getline(std::cin, line);
		std::vector<std::string> tokens = tokenize(line);
		
		if(!tokens.empty())
		{
			if(tokens[0].find("help") != std::string::npos)
				help();
			else if(tokens[0].find("exit") != std::string::npos ||  tokens[0].find("quit") != std::string::npos)
				break;
			else if(tokens[0].find("list") != std::string::npos)
				listDevices();
			else if(tokens[0].find("open") != std::string::npos ||  tokens[0] == "o")
			{
				if(tokens.size() > 1)
				{
					size_t index = std::stoul(tokens[1]);
					if(index < Camera::getAvailableCameras().size())
					{
						camera.openCamera(index);
						if(camera.isOpen())
						{
							camera.setAcquisitionMode(Camera::MODE_SINGLE);
							camera.setBayerMode(Camera::BAYER_GREEN);
							camera.setTriggerMode(Camera::TRIGGER_SOFTWARE);
							camera.startAcquisition();
						}
					}
					else 
					{
						std::cout<<"Index out of range\n";
					}
				}
			}
			else if(tokens[0].find("capture") != std::string::npos || tokens[0] == "c")
			{
				if(camera.isOpen())
				{
					if(tokens.size() > 1)
						filename = tokens[1] + ".png";
					else
						filename = "testimg.png";
					camera.startAcquisition();
					camera.trigger();
				}
				else
				{
					std::cout<<"Camera not open\n";
				}
			}
			else if(tokens[0].find("exposure") != std::string::npos || tokens[0] == "e")
			{
				if(camera.isOpen())
				{
					if(tokens.size() > 1)
					{
						if(!camera.setExposureTime(atof(tokens[1].c_str())*1000000.0))
							std::cout<<"setting exposure time failed\n";
					}
					else
					{
						uint64_t min, max;
						camera.getExposureTimeLimits(min, max);
						std::cout<<"Exposure time: "<<camera.getExposureTime()/1000000.0<<'\n'
						<<" Limits: "<<min/1000000.0<<" to "<<max/1000000.0<<'\n';
					}
				}
				else
				{
					std::cout<<"Camera not open\n";
				}
			}
			else if(tokens[0].find("gain") != std::string::npos || tokens[0] == "g")
			{
				if(camera.isOpen())
				{
					if(tokens.size() > 1)
					{
						if(!camera.setGain(atof(tokens[1].c_str())))
							std::cout<<"setting exposure time failed\n";
					}
					else
						std::cout<<"Gain: "<<camera.getGain()<<'\n';
				}
				else
				{
					std::cout<<"Camera not open\n";
				}
			}
			else
			{
				std::cout<<tokens[0]<<" is not a valid command\n";
			}
		}
	}
	return 0;
}

